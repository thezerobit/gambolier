#|
  This file is a part of gambolier project.
  Copyright (c) 2013 Stephen A. Goss (steveth45@gmail.com)
|#

(in-package :cl-user)
(defpackage gambolier-test
  (:use :cl
        :gambol
        :gambolier
        :cl-test-more)
  (:shadowing-import-from :cl-test-more :is)
  (:shadowing-import-from :gambol :fail))
(in-package :gambolier-test)

(plan nil)

(let ((rules (make-rulebase)))
  (with-rulebase rules
    ;; basic sanity test, adding FOO predicate
    (is (pl-solve-all '((foo ?x))) nil)
    (*- (foo bar))
    (*- (foo baz))
    (is (pl-solve-all '((foo ?x)))
        '(((?x . bar))
          ((?x . baz))))
    ;; load up them rules
    (add-rules rules)
    ;; =<
    (is (pl-solve-all '((=< 1 2))) '(t))
    (is (pl-solve-all '((=< 2 1))) nil)
    (is (pl-solve-all '((=< 2 2))) '(t))
    ;; <
    (is (pl-solve-all '((< 1 2))) '(t))
    (is (pl-solve-all '((< 2 1))) nil)
    (is (pl-solve-all '((< 2 2))) nil)
    ;; >=
    (is (pl-solve-all '((>= 1 2))) nil)
    (is (pl-solve-all '((>= 2 1))) '(t))
    (is (pl-solve-all '((>= 2 2))) '(t))

    ;; between/3
    (is (pl-solve-all '((between 1 3 ?n)))
        '(((?n . 1)) ((?n . 2)) ((?n . 3))))
    (is (pl-solve-all '((between 1 5 1))) '(t))
    (is (pl-solve-all '((between 1 5 2))) '(t))
    (is (pl-solve-all '((between 1 5 5))) '(t))
    (is (pl-solve-all '((between 1 5 0))) nil)
    (is (pl-solve-all '((between 1 5 6))) nil)

    ;; in-rect/6
    ;; checks if a point is in the rect
    (is (pl-solve-all '((in-rect 5 15 7 18 5 15))) '(t))
    ;; can produce all points in the rect
    (is (pl-solve-all '((in-rect 5 15 7 18 ?x ?y)))
        '(((?x . 5) (?y . 15)) ((?x . 5) (?y . 16)) ((?x . 5) (?y . 17))
          ((?x . 5) (?y . 18)) ((?x . 6) (?y . 15)) ((?x . 6) (?y . 16))
          ((?x . 6) (?y . 17)) ((?x . 6) (?y . 18)) ((?x . 7) (?y . 15))
          ((?x . 7) (?y . 16)) ((?x . 7) (?y . 17)) ((?x . 7) (?y . 18))))

    ;; insertion sort tests
    (is (pl-solve-all '((isort (3 1 2 6 5) ?x))) '(((?x 1 2 3 5 6))))
    (is (pl-solve-all '((isort1 (3 1 2 6 5) ?x))) '(((?x 1 2 3 5 6))))

    ;; plus
    (is (pl-solve-all '((plus ?x 4 10))) '(((?x . 6))))
    (is (pl-solve-all '((plus 6 ?x 10))) '(((?x . 4))))
    (is (pl-solve-all '((plus 6 4 ?x))) '(((?x . 10))))

    ;; map
    (is (pl-solve-all '((map (plus 5) (1 2 3) ?x))) '(((?x 6 7 8))))
    ;; filter
    (is (pl-solve-all '((filter (=< 10) (20 4 10 25 1) ?x)))
        '(((?x 20 10 25))))
    ;; foldr
    (is (pl-solve-all '((foldr (plus) 0 (1 2 3) ?x))) '(((?x . 6))))

    ;; from naish
    (is (pl-solve-all '((filter (> 5) (3 4 5 6 7) ?as)))
        '(((?as . (3 4)))))
    (is (pl-solve-all '((map (plus 1) (2 3 4) ?as)))
        '(((?as . (3 4 5)))))
    (is (pl-solve-all '((map (between 1) (2 3) ?as)))
        '(((?as 1 1)) ((?as 1 2)) ((?as 1 3)) ((?as 2 1)) ((?as 2 2))
          ((?as 2 3))))
    (is (pl-solve-all '((map (plus 1) ?as (3 4 5))))
        '(((?as . (2 3 4)))))
    (is (pl-solve-all '((map (plus ?x) (2 3 4) (3 4 5))))
        '(((?x . 1))))

    ))

(finalize)
