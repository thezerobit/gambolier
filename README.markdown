# Gambolier

A library of helpful Prolog predicates for cl-gambol, a minimal Prolog
implementation in Common Lisp. Requires my personal branch of cl-gambol
with some added goodies here:
[cl-gambol](https://github.com/deliciousrobots/cl-gambol)

## Status

Roughly 1% complete.

## Usage

Add rules to a rulebase with (ADD-RULES rulebase).

## Installation

Clone this repo into ~/quicklisp/local-projects and load
with quicklisp. (ql:quickload "gambolier")

## Author

* Stephen A. Goss (steveth45@gmail.com)

## Copyright

Copyright (c) 2013 Stephen A. Goss (steveth45@gmail.com)

# License

Licensed under the Modified BSD License.

