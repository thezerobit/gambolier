#|
  This file is a part of gambolier project.
  Copyright (c) 2013 Stephen A. Goss (steveth45@gmail.com)
|#

(in-package :cl-user)
(defpackage gambolier-test-asd
  (:use :cl :asdf))
(in-package :gambolier-test-asd)

(defsystem gambolier-test
  :author "Stephen A. Goss"
  :license "Modified BSD"
  :depends-on (:gambolier
               :cl-test-more)
  :components ((:module "t"
                :components
                ((:file "gambolier"))))
  :perform (load-op :after (op c) (asdf:clear-system c)))
