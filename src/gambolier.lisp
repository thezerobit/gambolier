#|
  This file is a part of gambolier project.
  Copyright (c) 2013 Stephen A. Goss (steveth45@gmail.com)
|#

(in-package :cl-user)
(defpackage gambolier
  (:use :cl :gambol)
  (:export #:add-rules
           #:=<
           #:between
           #:in-rect
           #:isort
           #:insert
           #:isort1
           #:number
           #:plus
           #:call
           #:filter
           #:foldr
           #:compose))
(in-package :gambolier)

(defun add-rules (rulebase)
  (with-rulebase rulebase
    ;; comparison operators
    (*- (=< ?n ?m) (lop (<= ?n ?m)))
    (*- (< ?n ?m) (lop (< ?n ?m)))
    (*- (>= ?n ?m) (lop (>= ?n ?m)))
    (*- (> ?n ?m) (lop (> ?n ?m)))
    ;; helpers
    (*- (between ?n ?m ?k) (=< ?n ?m) (= ?k ?n))
    (*- (between ?n ?m ?k) (< ?n ?m) (is ?n1 (lop (1+ ?n)))
        (between ?n1 ?m ?k))
    (*- (in-rect ?x1 ?y1 ?x2 ?y2 ?x ?y)
        (between ?x1 ?x2 ?x) (between ?y1 ?y2 ?y))
    ;; insertion sort algorithms
    (*- (isort nil nil))
    (*- (isort (?a . ?as) ?bs) (isort ?as ?bs1) (insert ?a ?bs1 ?bs))
    (*- (insert ?n nil (?n)))
    (*- (insert ?n (?h . ?l) (?n . (?h . ?l))) (=< ?n ?h))
    (*- (insert ?n (?h . ?l0) (?h . ?l)) (> ?n ?h) (insert ?n ?l0 ?l))
    (*- (isort1 ?as ?bs) (isort-a ?as nil ?bs))
    (*- (isort-a nil ?bs ?bs))
    (*- (isort-a (?a . ?as) ?bs ?cs)
        (insert ?a ?bs ?bs1) (isort-a ?as ?bs1 ?cs))
    ;; number
    (*- (number ?x) (nonvar ?x) (lop (numberp ?x)))
    ;; plus
    (*- (plus ?a ?b ?sum)
        (number ?a) (number ?b) (cut)
        (= ?sum (lop (+ ?a ?b))))
    (*- (plus ?a ?b ?sum)
        (number ?a) (number ?sum) (cut)
        (= ?b (lop (- ?sum ?a))))
    (*- (plus ?a ?b ?sum)
        (number ?b) (number ?sum) (cut)
        (= ?a (lop (- ?sum ?b))))

    ;; (append list1 list2 list-1+2)
    (*- (append (?x . ?y) ?z (?x . ?w)) (append ?y ?z ?w))
    (*- (append nil ?x ?x))

    ;; call
    (*- (call ?f) ?f)
    (*- (call ?f ?a) (append ?f (?a) ?args) ?args)
    (*- (call ?f ?a ?b) (append ?f (?a ?b) ?args) ?args)
    (*- (call ?f ?a ?b ?c) (append ?f (?a ?b ?c) ?args) ?args)
    (*- (call ?f ?a ?b ?c ?d) (append ?f (?a ?b ?c ?d) ?args) ?args)
    (*- (call ?f ?a ?b ?c ?d ?e) (append ?f (?a ?b ?c ?d ?e) ?args) ?args)

    ;; some higher order functions which require call/N
    ;; (map function inlist outlist)
    (*- (map ?? nil nil))
    (*- (map ?f (?a0 . ?as0) (?a . ?as))
        (call ?f ?a0 ?a)
        (map ?f ?as0 ?as))
    ;; (filter predicate inlist outlist)
    (*- (filter ?? nil nil))
    (*- (filter ?p (?a0 . ?as0) ?as)
        (call ?p ?a0)
        (cut)
        (= ?as (?a0 . ?as1))
        (filter ?p ?as0 ?as1))
    (*- (filter ?p (?a0 . ?as0) ?as)
        (= ?as ?as1)
        (filter ?p ?as0 ?as1))
    ;; (foldr function initial values result)
    (*- (foldr ?f ?b nil ?b))
    (*- (foldr ?f ?b (?a . ?as) ?r)
        (foldr ?f ?b ?as ?r1)
        (call ?f ?a ?r1 ?r))
    ;; (compose f g x (f (g x)))
    (*- (compose ?f ?g ?x ?fgx)
        (call ?g ?x ?gx)
        (call ?f ?gx ?fgx))
    ;; (converse f x y (f y x))
    (*- (converse ?f ?x ?y ?fyx)
        (call ?f ?y ?x ?fyx))
    ))

(defun reset-repl ()
  (clear-rules)
  (add-rules (current-rulebase)))

